<%@ include file="/Aluno/headAluno.jsp" %> 


<div class="row">
  <div class="col-md-12">
  	<div class="page-header">
    	<h4><strong>Área de Colaboração</strong></h4>
    </div>
  </div>
</div>
<div class = "row">
	<!-- Questão-->
<div class ="questao">
	<div class = "col-md-5">
		<div class="panel panel-yellow">
          <div class="panel-heading">
             	 <span class="glyphicon glyphicon-list-alt"></span> <strong>Questão</strong>
          </div>
	         <div class="panel-body">
				<ul class="media-list">
					<li class="media">
						<div class="media-body">
							<div class="media">
								<div class="media-body" >
									<h5 class="text-center text-uppercase"><strong> Enunciado</strong></h5>
							    	<p>(6.1) A confeitaria Hudson Valley fabrica sonhos que são ebalados 
							    	em pacotes com a indicação de que há 12 sonhos pesando um total de 42oz. 
							    	Se a variaçao entre os sonhos é muito grande, algumas caixas terão peso a 
							    	mais e outras a menos, o que desagrada o consumidor. O supervisor de controle 
							    	de qualidade constatou que esses problemas podem ser evitados se os sonhos tiverem
							    	 um peso médio de 3,5oz e um desvio-padrão de 0,06oz ou menos. Selecionaram-se 
							    	 aleatoriamente, na linha de produção, doze sonhos, que são pesados, dando os 
							    	 resultados abaixo. Com respeito a estimativa da variância e do desvio-padrão 
							    	 populacional e ao nível de confiança de 95% assinale as alternativas verdadeiras 
							    	 no modelo somatório: DADOS: 3,58 3,50 3,68 3,61 3,42 3,52 3,66 3,50 3,36 3,42
							    	 </p> 
							   		<small class="text-muted">FONTE: adaptado Triola</small>
								</div>
								<div class = 'media-footer'>
									<h5 class="text-center text-uppercase"><strong> Alternativas</strong></h5>
									<ul class="list-group">
									  <li class="list-group-item">X²r=3,816</li>
									  <li class="list-group-item">X²r=21,920</li>
									  <li class="list-group-item">O intervalo de confiança para a variância esta entre 0,006 e 0,034</li>
									  <li class="list-group-item">O intervalo de confiança para a variância esta entre 0,77 e 0,185</li>
									  <li class="list-group-item">O intervalo de confiança para o desvio-padrão esta entre 0,77 e 0,185</li>
									</ul>	
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class = "panel-footer">
				<div class = "row">
					<div class = "text-center">
						<button class="btn btn-warning btn-sm" id="votacao" data-toggle="modal" data-target="#modalVotacao">Responder <span class="glyphicon glyphicon-ok-circle"></span></button>
						<button class="btn btn-default btn-sm" id="Proximo">Proximo <span class="glyphicon glyphicon-arrow-right"></span></button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



	<!-- Chat -->
<section id = "chat">
    <div class="col-md-5">
    	<div class="panel panel-default">
			<div class="panel-heading">
			    <span class="glyphicon glyphicon-comment"></span> <strong>Chat</strong>
			    
			    <span class="glyphicon glyphicon-time"></span><strong> Restam: 05:00 </strong>
			    
			    <div class="btn-group pull-right">
			        <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown">
			            Enviar <span class="glyphicon glyphicon-chevron-down"></span>
			        </button>
			        <ul class="dropdown-menu slidedown">
			            <li><a href="#"><span class="glyphicon glyphicon-file"></span> Arquivo</a></li>
			            <li><a href="#"><span class="glyphicon glyphicon-picture"></span> Imagem</a></li>
			        </ul>
			    </div>
			</div>
			<div class="panel-body">
			    <ul class="chat">
			        <li class="left clearfix"><span class="chat-img pull-left">
			            <img src="http://placehold.it/50/55C1E7/fff&amp;text=U" alt="User Avatar" class="img-circle">
			        </span>
			            <div class="chat-body clearfix">
			                <div class="header">
			                    <strong class="primary-font">Lucas Novelli</strong> <small class="pull-right text-muted">
			                        <span class="glyphicon glyphicon-time"></span>17 mins ago</small>
			                </div>
			                <p>
			                   Acho que você calculou de forma incorreta.
			                </p>
			            </div>
			        </li>
			        <li class="left clearfix"><span class="chat-img pull-left">
			            <img src="http://placehold.it/50/55C1E7/fff&amp;text=U" alt="User Avatar" class="img-circle">
			        </span>
			            <div class="chat-body clearfix">
			                <div class="header">
			                    <strong class="primary-font">Lucas Novelli</strong> <small class="pull-right text-muted">
			                        <span class="glyphicon glyphicon-time"></span>16 mins ago</small>
			                </div>
			                <p>
			                    Porque os meus resultados estão diferentes, essa multiplicação sua está um pouco estranha.
			                    Você não acha?
			                </p>
			            </div>
			        </li>
			        <li class="left clearfix"><span class="chat-img pull-left">
			            <img src="http://placehold.it/50/55C1E7/fff&amp;text=U" alt="User Avatar" class="img-circle">
			        </span>
			            <div class="chat-body clearfix">
			                <div class="header">
			                    <strong class="primary-font">Lucas Novelli</strong> <small class="pull-right text-muted">
			                        <span class="glyphicon glyphicon-time"></span>15 mins ago</small>
			                </div>
			                <p>
			                    Aparece um resultado negativo, mas no problema não existe dados negativos. De onde surgiu esse negativo?
			                </p>
			            </div>
			        </li>
			        <li class="right clearfix"><span class="chat-img pull-right">
			            <img src="http://placehold.it/50/FA6F57/fff&amp;text=ME" alt="User Avatar" class="img-circle">
			        </span>
			            <div class="chat-body clearfix">
			                <div class="header">
			                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>
			                    <strong class="pull-right primary-font">Eliana Ishikawa</strong>
			                </div>
			                <p>
			                    Na verdade você está utilizando uma fórmula que não se aplica a este exercício.
			                </p>
			            </div>
			        </li>
			        <li class="left clearfix"><span class="chat-img pull-left">
			            <img src="http://placehold.it/50/55C1E7/fff&amp;text=U" alt="User Avatar" class="img-circle">
			        </span>
			            <div class="chat-body clearfix">
			                <div class="header">
			                    <strong class="primary-font">Lucas Novelli</strong> <small class="pull-right text-muted">
			                        <span class="glyphicon glyphicon-time"></span>10 mins ago</small>
			                </div>
			                <p>
			                   E qual seria a correta?
			                </p>
			            </div>
			        </li>
			        <li class="right clearfix"><span class="chat-img pull-right">
			            <img src="http://placehold.it/50/FA6F57/fff&amp;text=ME" alt="User Avatar" class="img-circle">
			        </span>
			            <div class="chat-body clearfix">
			                <div class="header">
			                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>10 mins ago</small>
			                    <strong class="pull-right primary-font">Eliana Ishikawa</strong>
			                </div>
			                <p>
			                    É a que está na página 105 do livro. Esse é o motivo que nossos resultados estão diferentes!
			                    Utilize a formula dessa página e veja como meus cálculos estão corretos.
			                </p>
			            </div>
			        </li>
			        <li>
				        <div class="input-group">
					        <small class="text-center">
					        <span class="glyphicon glyphicon-time"></span>Você está há 10 minutos sem colaborar!</small>
				    	</div>
			        </li>
			    </ul>
			</div>
			<div class="panel-footer">
			    <div class="input-group">
			        <input id="btn-input" class="form-control input-sm" placeholder="Escreva sua mensagem..." type="text">
			        <span class="input-group-btn">
			            <button class="btn btn-warning btn-sm" id="btn-chat">
			                Enviar  <span class="glyphicon glyphicon-send"></span></button>
			        </span>
			     </div>
			</div>
		</div>
		<!-- mostra as opções relativas ao celular -->	
		<div class = "opcoesCelular">
			<div class = "row">
				<div class = "col-sm-12 col-xs-12">
					<div class = "text-center">
						<div class="btn-group">
							<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#Online">Online</button>
							<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalQuestao">Questão</button>
							<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modalVotacao">Responder</button>
							<button type="button" class="btn btn-warning">Pular</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
	<br>
	<br>
	<br>
	</div>
</section>

<div class ="usuario">	
<!-- Painel de usuarios ativos na visualização desktop-->
<div class = "col-md-2">
	<div class="panel panel-default">
          <div class="panel-heading">
             <strong>Usuários Online</strong>
          </div>
         <div class="panel-body">
             <ul class="media-list">
				<li class="media">
					<div class="media-body">
						<div class="media">
							<a class="pull-left" href="#">
					    	<img class="media-object img-circle" style="max-height:50px;" src="https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-128.png" />
							</a>
							<div class="media-body" >
						    	<h5>Lucas Novelli </h5>
						   		<h6><span class="label label-success">Online</span></h6>
							</div>
						</div>
					</div>
				</li>
				<li class="media">
					<div class="media-body">
						<div class="media">
							<a class="pull-left" href="#">
					    	<img class="media-object img-circle" style="max-height:50px;" src="https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-128.png" />
							</a>
							<div class="media-body" >
						    	<h5>Eliana Ishik...</h5>
						   		<h6><span class="label label-success">Online</span></h6>
							</div>
						</div>
					</div>
				</li>
				<li class="media">
					<div class="media-body">
						<div class="media">
							<a class="pull-left" href="#">
					    	<img class="media-object img-circle" style="max-height:50px;" src="https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-128.png" />
							</a>
							<div class="media-body" >
						    	<h5>Simone Nasser</h5>
						   		<h6><span class="label label-danger">Offline</span></h6>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
		<div class = "panel-footer">
			<div class = "row">
				<div class = "text-center">
				</div>
			</div>
		</div>
	</div>
</div>
</div>


</div>
</div></div>
</div>

<!-- Modal para a apresentação da questão no modo celular -->
<div class="modal fade" id = "modalQuestao">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Questao</h4>
      </div>
      <div class="modal-body">
        <div class = "row">
        	<div class="col-sm-12">
		    	<div class="panel panel-yellow">
				         <div class="panel-body">
							<ul class="media-list">
								<li class="media">
									<div class="media-body">
										<div class="media">
											<div class="media-body" >
												<h5 class="text-center text-uppercase"><strong> Enunciado</strong></h5>
										    	<p>(6.1) A confeitaria Hudson Valley fabrica sonhos que são ebalados 
										    	em pacotes com a indicação de que há 12 sonhos pesando um total de 42oz. 
										    	Se a variaçao entre os sonhos é muito grande, algumas caixas terão peso a 
										    	mais e outras a menos, o que desagrada o consumidor. O supervisor de controle 
										    	de qualidade constatou que esses problemas podem ser evitados se os sonhos tiverem
										    	 um peso médio de 3,5oz e um desvio-padrão de 0,06oz ou menos. Selecionaram-se 
										    	 aleatoriamente, na linha de produção, doze sonhos, que são pesados, dando os 
										    	 resultados abaixo. Com respeito a estimativa da variância e do desvio-padrão 
										    	 populacional e ao nível de confiança de 95% assinale as alternativas verdadeiras 
										    	 no modelo somatório: DADOS: 3,58 3,50 3,68 3,61 3,42 3,52 3,66 3,50 3,36 3,42
										    	 </p> 
										   		<small class="text-muted">FONTE: adaptado Triola</small>
											</div>
											<div class = 'media-footer'>
												<h5 class="text-center text-uppercase"><strong> Alternativas</strong></h5>
												<ul class="list-group">
												  <li class="list-group-item">X²r=3,816</li>
												  <li class="list-group-item">X²r=21,920</li>
												  <li class="list-group-item">O intervalo de confiança para a variância esta entre 0,006 e 0,034</li>
												  <li class="list-group-item">O intervalo de confiança para a variância esta entre 0,77 e 0,185</li>
												  <li class="list-group-item">O intervalo de confiança para o desvio-padrão esta entre 0,77 e 0,185</li>
												</ul>	
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class = "panel-footer">
							<div class = "row">
								<div class = "text-center">
									<button class="btn btn-warning btn-sm" id="votacaoCEl"  data-toggle="modal" data-dismiss="modal" data-target="#modalVotacao">Responder</button>
								</div>
							</div>
						</div>
				</div>
			</div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal para a apresentação da votação -->
<div class="modal fade" id = "modalVotacao">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Votação</h4>
      </div>
      <div class="modal-body">
        <div class = "row">
        	<div class="col-sm-12">
		    	<div class="panel panel-yellow">
				         <div class="panel-body">
							<ul class="media-list">
								<li class="media">
									<div class="media-body">
										<div class="media">
											<div class="media-body" >
												<h5 class="text-center text-uppercase"><strong> Enunciado</strong></h5>
										    	<p>(6.1) A confeitaria Hudson Valley fabrica sonhos que são ebalados 
										    	em pacotes com a indicação de que há 12 sonhos pesando um total de 42oz. 
										    	Se a variaçao entre os sonhos é muito grande, algumas caixas terão peso a 
										    	mais e outras a menos, o que desagrada o consumidor. O supervisor de controle 
										    	de qualidade constatou que esses problemas podem ser evitados se os sonhos tiverem
										    	 um peso médio de 3,5oz e um desvio-padrão de 0,06oz ou menos. Selecionaram-se 
										    	 aleatoriamente, na linha de produção, doze sonhos, que são pesados, dando os 
										    	 resultados abaixo. Com respeito a estimativa da variância e do desvio-padrão 
										    	 populacional e ao nível de confiança de 95% assinale as alternativas verdadeiras 
										    	 no modelo somatório: DADOS: 3,58 3,50 3,68 3,61 3,42 3,52 3,66 3,50 3,36 3,42
										    	 </p> 
										   		<small class="text-muted">FONTE: adaptado Triola</small>
											</div>
											<div class = 'media-footer'>
												<h5 class="text-center text-uppercase"><strong> Alternativas</strong></h5>
												<div class = "text-center">
													<div class="row">
													<!-- Opções -->
												  		<div class="col-sm-12 col-md-12 col-lg-12">
													    	<div class="input-group">
													      		<span class="input-group-addon">
														        	<input type="checkbox">
														      	</span>
													      		<input type="text" class="form-control" value ="X²r=3,816" readonly>
														    </div>
														    <div class="input-group">
													      		<span class="input-group-addon">
												        			<input type="checkbox" >
												      			</span>
													      		<input type="text" class="form-control"  value ="X²r=21,920" readonly>
														    </div>
														    <div class="input-group">
												      			<span class="input-group-addon">
												        			<input type="checkbox" >
												      			</span>
												      			<input type="text" class="form-control"  value ="O intervalo de confiança para a variância esta entre 0,006 e 0,034" readonly>
											    			</div>
														    <div class="input-group">
														      <span class="input-group-addon">
												        		<input type="checkbox" >
														      </span>
														      <input type="text" class="form-control"  value ="O intervalo de confiança para a variância esta entre 0,77 e 0,185" readonly>
														    </div>
														    <div class="input-group">
														      <span class="input-group-addon">
												        			<input type="checkbox" >
														      </span>
														      <input type="text" class="form-control"  value =" O intervalo de confiança para o desvio-padrão esta entre 0,77 e 0,185" readonly>
														    </div>
														</div><!-- col-->
													</div><!-- /.row -->	
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class = "panel-footer">
							<div class = "row">
								<div class = "text-center">
									<button class="btn btn-warning" id="confirmar" data-toggle="modal" data-dismiss="modal" data-target="#empate">Confirmar</button>
								</div>
							</div>
						</div>
				</div>
			</div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- Modal para apresentação dos participantes online no modo celular -->
<div class="modal fade" id = "correcao"tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title text-center text-uppercase"><strong>Correção <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span></strong></h4>
      		</div>
      		<div class="modal-body">
        		<div class="panel panel-default">
		         	<div class="panel-body">
		         		<div class = "row">
	            				<div class = "col-md-12 col-sm-12 text-center">
		            				<h4 class="text-uppercase"><strong>Resolução</strong></h4>
		            				<p>Resolução para o exercício proposto.</p>
	            				</div>
	            			</div>
	            				<div class = "col-md-12 col-sm-12">
	            					<div class="row">
	            					<hr>
	            				</div>
	            			</div>
	            			<div class="row">
							<!-- Opções -->
							<!-- Resposta Correta -->
						  		<div class="col-sm-6 col-md-6">
						  			<h5 class="text-center text-uppercase"><strong> <span class = "label label-success">Resposta Correta</span></strong></h5>
						  			<ul class="list-group">
									  <li class="list-group-item">X²r=3,816</li>
									  <li class="list-group-item">X²r=21,920</li>
									  <li class="list-group-item">O intervalo de confiança para a variância esta entre 0,77 e 0,185</li>
									</ul>
					  			</div>
					  			<!-- Resposta 1 -->
					  			<div class="col-sm-6 col-md-6">
						  			<h5 class="text-center text-uppercase"><strong> Explicação</strong></h5>
						  			<p>Explicação</p>
								</div><!-- col-->
							</div><!-- /.row  das respostas-->	
					</div>
				</div>
      		</div>
      		<div class ="modal-footer">
      			<div class="text-center">
      				<button type="button" class="btn btn-warning" data-dismiss="modal">Avançar</button>
      			</div>
      		</div>
    	</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Modal para apresentação da resposta correta -->
<div class="modal fade" id = "Online" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title text-center">Usuários Online</h4>
      		</div>
      		<div class="modal-body">
        		<ul class="media-list">
				<li class="media">
					<div class="media-body">
						<div class="media">
							<a class="pull-left" href="#">
					    	<img class="media-object img-circle" style="max-height:50px;" src="https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-128.png" />
							</a>
							<div class="media-body" >
						    	<h5>Lucas Novelli </h5>
						   		<h6><span class="label label-success">Online</span></h6>
							</div>
						</div>
					</div>
				</li>
				<li class="media">
					<div class="media-body">
						<div class="media">
							<a class="pull-left" href="#">
					    	<img class="media-object img-circle" style="max-height:50px;" src="https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-128.png" />
							</a>
							<div class="media-body" >
						    	<h5>Eliana Ishikawa</h5>
						   		<h6><span class="label label-success">Online</span></h6>
							</div>
						</div>
					</div>
				</li>
				<li class="media">
					<div class="media-body">
						<div class="media">
							<a class="pull-left" href="#">
					    	<img class="media-object img-circle" style="max-height:50px;" src="https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-128.png" />
							</a>
							<div class="media-body" >
						    	<h5>Simone Nasser</h5>
						   		<h6><span class="label label-danger">Offline</span></h6>
							</div>
						</div>
					</div>
				</li>
			</ul>
      		</div>
    	</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal para apresentação de empate da escolhe da alternativa correta -->
<div class="modal fade" id = "empate" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title text-center text-uppercase"><strong>Atenção <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span></strong></h4>
      		</div>
      		<div class="modal-body">
        		<div class="panel panel-default">
		         	<div class="panel-body">
	            		<section id="panelEmpate">
	            			<div class = "row">
	            				<div class = "col-md-12 col-sm-12 text-center">
		            				<h4 class="text-uppercase"><strong> Empate</strong></h4>
		            				<p> Houveram respostas distintas para a exercício!
		            					Continuem a colaborar!</p>
	            				</div>
	            			</div>
	            				<div class = "col-md-12 col-sm-12">
	            					<div class="row">
	            				</div>
	            			<hr>
	            			</div>
	            			<div class="row">
							<!-- Opções -->
							<!-- Resposta 1 -->
						  		<div class="col-sm-6 col-md-6">
						  			<h5 class="text-center text-uppercase"><strong> Resposta 1</strong></h5>
						  			<ul class="list-group">
									  <li class="list-group-item">X²r=3,816</li>
									  <li class="list-group-item">X²r=21,920</li>
									  <li class="list-group-item">O intervalo de confiança para a variância esta entre 0,77 e 0,185</li>
									</ul>
					  			</div>
					  			<!-- Resposta 1 -->
					  			<div class="col-sm-6 col-md-6">
						  			<h5 class="text-center text-uppercase"><strong> Resposta 2</strong></h5>
						  			<ul class="list-group">
									  <li class="list-group-item">X²r=3,816</li>
									  <li class="list-group-item">O intervalo de confiança para a variância esta entre 0,006 e 0,034</li>
									  <li class="list-group-item">O intervalo de confiança para o desvio-padrão esta entre 0,77 e 0,185</li>
									</ul>
								</div><!-- col-->
							</div><!-- /.row  das respostas-->	
	            		</section>
					</div><!-- panel body -->
				</div><!-- panel -->
      		</div><!-- modal body -->
      		<div class ="modal-footer">
      			<div class="text-center">
      				<button type="button" class="btn btn-warning" id="OKEmpate" data-toggle="modal" data-dismiss="modal" data-target="#correcao">Escolher correta</button>
      			</div>
      		</div>
    	</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>