package modelObjects;

public class Proposicao {
	
	private int idProposicao;
	private String enunciado, imagem, numero;
	private boolean correta;
	
	public int getIdProposicao() {
		return idProposicao;
	}
	public void setIdProposicao(int idProposicao) {
		this.idProposicao = idProposicao;
	}
	public String getEnunciado() {
		return enunciado;
	}
	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public boolean isCorreta() {
		return correta;
	}
	public void setCorreta(boolean correta) {
		this.correta = correta;
	}
	
	@Override
	public String toString(){
		return "id "+this.idProposicao+" enunciado "+this.enunciado;
		
	}
}
