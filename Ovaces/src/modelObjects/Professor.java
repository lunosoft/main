package modelObjects;

public class Professor extends Usuario{
	
	private String registro;
	
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	private int id;
	
	public Integer getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

}
