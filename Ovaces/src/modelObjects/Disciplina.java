package modelObjects;

public class Disciplina {
	
	private int idDisicplina;
	private String nome, codigo;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Integer getIdDisicplina() {
		return idDisicplina;
	}
	public void setIdDisicplina(int idDisicplina) {
		this.idDisicplina = idDisicplina;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}

}
