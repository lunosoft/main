package modelObjects;

import java.util.List;

public class Questao {
	
	private Integer idQuestao;
	private String dificuldade, imagem, enunciado; 
	private double peso;
	private List<Proposicao> proposicoes;
	private Referencia referencia;
	private Conteudo conteudo;
	
	public Integer getIdQuestao() {
		return idQuestao;
	}
	public void setIdQuestao(int idQuestao) {
		this.idQuestao = idQuestao;
	}
	
	public String getDificuldade() {
		return dificuldade;
	}
	public void setDificuldade(String dificuldade) {
		this.dificuldade = dificuldade;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getEnunciado() {
		return enunciado;
	}
	public void setEnunciado(String enunciado) {
		this.enunciado = enunciado;
	}
	public Double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public List<Proposicao> getProposicoes() {
		return proposicoes;
	}
	public void setProposicoes(List<Proposicao> proposicoes) {
		this.proposicoes = proposicoes;
	}
	public Referencia getReferencia() {
		return referencia;
	}
	public void setReferencia(Referencia referencia) {
		this.referencia = referencia;
	}
	public Conteudo getConteudo() {
		return conteudo;
	}
	public void setConteudo(Conteudo conteudo) {
		this.conteudo = conteudo;
	}
	
	@Override
	public String toString(){
		
		return "quest�o "+this.idQuestao+" enunciado "+this.enunciado+" proposicao "+this.proposicoes;
		
	}
	
}
