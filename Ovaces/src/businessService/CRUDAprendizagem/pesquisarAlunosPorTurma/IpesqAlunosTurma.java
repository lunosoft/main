package businessService.CRUDAprendizagem.pesquisarAlunosPorTurma;

import java.sql.ResultSet;
import java.util.ArrayList;

import modelObjects.Aluno;
import modelObjects.Turma;


public interface IpesqAlunosTurma {
	
	public ArrayList <Aluno> pequisarPorTurma(Turma turma);
	
	public ArrayList <Aluno> transformar(ResultSet rs);

}
