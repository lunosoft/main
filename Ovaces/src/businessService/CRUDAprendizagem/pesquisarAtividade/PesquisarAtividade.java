package businessService.CRUDAprendizagem.pesquisarAtividade;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import businessService.CRUDAprendizagem.pesquisarQuestao.IPesquisarQuestao;
import businessService.dbConnection.persistencia.IPersistencia;
import modelObjects.Atividade;



public class PesquisarAtividade implements IpesqAtividade, IPersistencia, IPesquisarQuestao{
	private String query;
	private Atividade aux;
	public Atividade pesquisar(int id){
		
		if(id<1) return null;
		
		query = "select * from atividade where idatividade = "+id+";";
		
		aux = this.transformar(pers.ExecuteQuery(query));
		
		query = "select idquestao from questaoatividade where idatividade = "+id+";";
		
		List<Integer> ids = this.transformarID(pers.ExecuteQuery(query));
		aux.setQuestoes(new ArrayList<>());
		
		for(Integer valor:ids){
			
			aux.getQuestoes().add(pesqQuestao.pesquisar(valor));
			
		}
		
		return aux;
		
	}
	
	public List<Atividade> pesquisarAll(){
		
		this.query = "select * from atividade;";
		
		
		ResultSet rs = pers.ExecuteQuery(query);
		List<Atividade> lista = new ArrayList<>();
		Atividade atividade;
		
		try{
			while(rs.next()){
				
				atividade = new Atividade();
				
				atividade.setFim(rs.getDate("datafim"));
				atividade.setIdAtividade(rs.getInt("idatividade"));
				atividade.setInicio(rs.getDate("datainicio"));
				atividade.setNome(rs.getString("nome"));
				
				lista.add(atividade);
				
			}
		}
		catch(SQLException e){
				e.printStackTrace();
				return null;
		}
		
		return lista;
		
	}
	
	public Atividade pesquisar(Atividade atividade){
		
		if (atividade == null || atividade.getNome().isEmpty()) return null;
		
		this.query = "select * from atividade where nome = "+atividade.getNome()+";";
		
		aux = this.transformar(pers.ExecuteQuery(query));
		
		query = "select idquestao from questaoatividade where idatividade = "+aux.getIdAtividade()+";";
		
		List<Integer> ids = this.transformarID(pers.ExecuteQuery(query));
		aux.setQuestoes(new ArrayList<>());
		
		for(Integer valor:ids){
			
			aux.getQuestoes().add(pesqQuestao.pesquisar(valor));
			
		}
		
		return aux;
	}
	
	public Atividade transformar(ResultSet rs){
		
		Atividade atividade;
		if(rs == null) return null;
		
		try{
			if(rs.next()){
				
				atividade = new Atividade();
				
				atividade.setFim(rs.getDate("datafim"));
				atividade.setIdAtividade(rs.getInt("idatividade"));
				atividade.setInicio(rs.getDate("datainicio"));
				atividade.setNome(rs.getString("nome"));
				
				return atividade;
				
			}
		}
		catch(SQLException e){
				e.printStackTrace();
				return null;
		}
		
		return null;
	}
	
	private List<Integer> transformarID(ResultSet rs){
		
		List<Integer> ids = new ArrayList<>();
		if(rs == null) return null;
		
		try{
			while(rs.next()){
				
				ids.add(rs.getInt(1));
				
			}
		}
		catch(SQLException e){
				e.printStackTrace();
				return null;
		}
		
		return ids;
	}
}
