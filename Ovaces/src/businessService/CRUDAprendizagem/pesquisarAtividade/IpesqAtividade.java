package businessService.CRUDAprendizagem.pesquisarAtividade;

import java.sql.ResultSet;

import modelObjects.Atividade;

public interface IpesqAtividade {
	
	public Atividade pesquisar(int id);
	
	public Atividade pesquisar(Atividade atividade);
	
	public Atividade transformar(ResultSet rs);
}
