package businessService.dbConnection.persistencia;

import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import businessService.dbConnection.conexao.IConectar;


public class Persistir implements IPersistir, IConectar {
	
	private Connection con;
	private Statement st;
	private ResultSet rs;
	
public int ExecuteUpdate(String query){
	
	 con = c.conectar();
     
     try{
         
         st = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                 ResultSet.CONCUR_READ_ONLY);
          
         return st.executeUpdate(query);
         
     }catch(SQLException e){
    	 
         System.out.printf("erro update %s\n", e);
         e.printStackTrace();
         return -1;           
     }
     finally{
         c.desconectar();
     }
}
	
	public ResultSet ExecuteQuery(String query){
		
		con = c.conectar();
		//System.out.println("conectar");
	
        try{
            
            st = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            
            //System.out.println("try");
            return  st.executeQuery(query);
            
            
        }catch(SQLException e){
            System.out.printf("erro de execute %s\n", e);
             e.printStackTrace();
        }
        finally{
        	//System.out.println("finally");
            c.desconectar();
        }
        return rs;
	}

}
